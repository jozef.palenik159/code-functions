<?php

namespace App\Controller;

use App\Entity\AbstractApplicationForm;
use App\Entity\Claim;
use App\Enum\ApplicationFormStatusEnum;
use App\Enum\ClaimReasonEnum;
use App\Enum\ClaimStatusEnum;
use App\Exception\NotAllowedException;
use App\Exception\NotFoundException;
use App\Exception\ServiceException;
use App\Factory\ClaimFactory;
use App\Form\ClaimType;
use App\Form\Data\ClaimData;
use App\Repository\ApplicationFormRepository;
use App\Repository\AttachmentRepository;
use App\Repository\ClaimRepository;
use App\Service\ApplicationForm\ApplicationFormService;
use App\Service\AttachmentService;
use App\Service\ClaimService;
use App\Service\DownloadService;
use App\Service\NotificationService;
use App\Service\PdfModuleService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Hateoas\Configuration\Route as HateoasRoute;
use Hateoas\Representation\CollectionRepresentation;
use Hateoas\Representation\PaginatedRepresentation;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ApiClaimController - for managing claims for application form
 *
 * @package App\Controller
 *
 * IsGranted("ROLE_APPLICATION_FORM_CLAIM") // TODO: Also you can control access in security.yaml
 *
 * @Rest\Version("v1")
 * @SWG\Tag(name="Reklamacie REST API", description="Reklamacie")
 */
class ApiClaimController extends AbstractFOSRestController
{
    /** @var ApplicationFormService $applicationFormService */
    private $applicationFormService;

    /** @var ClaimService $em */
    private $claimService;

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var ApplicationFormRepository */
    private $applicationRepository;

    /** @var ClaimRepository */
    private $claimRepository;

    /** @var AttachmentRepository */
    private $attachmentRepository;

    /** @var TranslatorInterface $translator */
    private $translator;

    /** @var ClaimFactory $claimFactory */
    private $claimFactory;

    /** @var LoggerInterface $log */
    private $logger;

    /** @var NotificationService $notificationsService */
    private $notificationsService;

    /** @var DownloadService $downloadService */
    private $downloadService;

    /**
     * @var AttachmentService
     */
    private $attachmentService;

    /**
     * ApiClaimController constructor.
     * @param ApplicationFormService $applicationFormService
     * @param ClaimService $claimService
     * @param EntityManagerInterface $em
     * @param ApplicationFormRepository $_applicationRepository
     * @param ClaimRepository $_claimRepository
     * @param TranslatorInterface $translator
     * @param LoggerInterface $logger
     * @param ClaimFactory $claimFactory
     * @param AttachmentRepository $attachmentRepository
     * @param NotificationService $notificationsService
     * @param DownloadService $downloadService
     * @param AttachmentService $attachmentService
     */
    public function __construct(
        ApplicationFormService $applicationFormService,
        ClaimService $claimService,
        EntityManagerInterface $em,
        ApplicationFormRepository $_applicationRepository,
        ClaimRepository $_claimRepository,
        TranslatorInterface $translator,
        LoggerInterface $logger,
        ClaimFactory $claimFactory,
        AttachmentRepository $attachmentRepository,
        NotificationService $notificationsService,
        DownloadService $downloadService,
        AttachmentService $attachmentService
    ) {
        $this->applicationFormService = $applicationFormService;
        $this->claimService = $claimService;
        $this->applicationRepository = $_applicationRepository;
        $this->claimRepository = $_claimRepository;
        $this->em = $em;
        $this->translator = $translator;
        $this->logger = $logger;
        $this->claimFactory = $claimFactory;
        $this->attachmentRepository = $attachmentRepository;
        $this->notificationsService = $notificationsService;
        $this->downloadService = $downloadService;
        $this->attachmentService = $attachmentService;
    }

    /**
     * Create claim from application
     *
     * Example Request
     * =============================
     *      GET "v1/requests/{uniqId}/claim"
     *
     * @Rest\Get("v1/requests/{uniqId}/claim", name="request_claim_form")
     * @param Request $request
     *
     * @return Response
     * @throws NotAllowedException
     * @throws NotFoundException
     */
    public function getApplicationClaimForm(Request $request, AbstractApplicationForm $applicationForm): Response
    {
        $templatesPrefix = "";
        if ($request->get('source') == 'wp') {
            $templatesPrefix = 'wp/';
        }
        if ($applicationForm->isInClaimStatus()) {
            $claim = $this->claimRepository->findOneByApplicationId($applicationForm->getId());
            if (empty($claim)) {
                throw new NotFoundException("Claim not found by id " . $applicationForm->getId());
            }
            $options = [
                'data' => $claim[0],
                'applicationForm' => $applicationForm
            ];
            $options['source'] = $this->isRequestFromWordpress($request) ? "wp" : "";

            return $this->handleView(
                $this->view(
                    [
                        "html" =>
                            [
                                "application_module_detail" =>
                                    $this->render(
                                        "{$templatesPrefix}json/claim_detail_json.html.twig",
                                        $options
                                    )->getContent()
                            ]
                    ],
                    Response::HTTP_OK,
                )
            );
        }
        if (
            !$applicationForm->canCreateClaim()
        ) {
            throw new NotAllowedException($this->translator->trans("errors.claim_not_in_valid_state"));
        }

        $dto = $this->claimFactory->getClaimFromApplication($applicationForm);
        $form = $this->createForm(ClaimType::class, $dto, [
            'application' => $applicationForm,
            'form_id' => 'application_form',
        ]);

        return $this->handleView(
            $this->view(
                [
                    "html" =>
                        [
                            "application_module_detail" =>
                                $this->render(
                                    "{$templatesPrefix}form/application_form_claim.html.twig",
                                    [
                                        'data' => [
                                            'id' => $applicationForm->getId(),
                                            'application' => $applicationForm,
                                            'server_url' =>
                                                (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'
                                                    ? "https" : "http") . "://$_SERVER[HTTP_HOST]",
                                        ],
                                        'form' => $form->createView()
                                    ]
                                )->getContent()
                        ],
                    'css' =>
                        [
                            'https://rawgit.com/enyo/dropzone/master/dist/dropzone.css',
                        ],
                    'js' =>
                        [
                            'https://rawgit.com/enyo/dropzone/master/dist/dropzone.js' => 'dropzoneLoaded',
                        ]
                ],
                Response::HTTP_OK,
            )
        );
    }

    /**
     * @Rest\Post("v1/requests/{uniqId}/claim", name="create_claim_form")
     *
     * @param AbstractApplicationForm $application
     * @param Request                 $request
     * @param PdfModuleService        $pdfModuleService
     * @param RouterInterface         $router
     * @return Response
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws ServiceException
     * @throws TransportExceptionInterface
     */
    public function saveApplicationClaimForm(
        AbstractApplicationForm $application,
        Request $request,
        PdfModuleService $pdfModuleService,
        RouterInterface $router
    ): Response {
        $templatesPrefix = $this->isRequestFromWordpress($request) ? "wp/" : "";
        $claim = new ClaimData();
        $form = $this->createForm(ClaimType::class, $claim, [
            'application' => $application,
            'form_id' => 'claim',
        ]);

        $formData = $request->get('claim');
        $claim = $this->claimFactory->getClaimFromRequest($formData, $application);
        $form->handleRequest($request);
        $errors = $form->getErrors(true, true);

        if (count($errors) != 0) {
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            // Allow all websites
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->setContent(json_encode(
                [
                    'javascriptEval' => preg_replace(
                        '/<[\/]*script[^>]*\>/i',
                        '',
                        $this->render(
                            "{$templatesPrefix}form/form_page_one_error_json.html.twig",
                            [
                                'errors' => $errors,
                                'form_id' => 'claim-form',
                                'form_name' => 'claim-form',
                            ]
                        )->getContent()
                    )
                ]
            ));

            return $response;
        }


        $this->em->transactional(function (EntityManager $em) use ($application, $claim) {
            $application->setStatus(ApplicationFormStatusEnum::CLAIM_PROCESSING);
            $em->persist($application);
            $claim->setStatus(ClaimStatusEnum::PROCESSING);
            $claim->setApplication($application);
            $em->persist($claim);
        });

        if( $application->hasApplicantEmail() ){
            //send confirmation to user
            $this->notificationsService->sendClaimConfirmation(
                $application,
                $application->getApplicantEmail()
            );
        }

        //send confirmation to admin
        $this->notificationsService->sendClaimNotificationToAdmin(
            $application,
            $claim->getId(),
            $this->translator->trans('claim_reason.'.$claim->getReasonForClaim()),
            $claim->getCreatedAt()->format('Y-m-d')
        );

        foreach ($request->request->get('files') as $file) {
            $moveClaimFile = $this->attachmentService->moveUploadClaimAttachment($file, $claim->getId());
            $this->attachmentService->saveClaimAttachment(
                $claim,
                $application,
                $moveClaimFile,
                'Priloha_reklamacie'
            );
        }
        $pdfModuleService->generateDocumentForClaim("pdf", $application, $claim);

        $isFromApplicationList = strpos($request->headers->get('referer'), 'detail-ziadosti') ? false : true;
        $isFromSsoUserCreate = false;
        if ($request->get('source') != 'wp') {
            // TODO: Temporary until claim is moved to administration. Should be implemented very soon!
            $detailButtonTarget = $router
                ->generate("admin_module_application_form_detail", ['number' => $application->getNumber()]);
        } else {
            if($isFromApplicationList){
                $isFromSsoUserCreate = true;
            }
            $detailButtonTarget = "";
        }

        $claimReasonEnum = new ClaimReasonEnum($claim->getReasonForClaim());
        if ($claimReasonEnum->equals(ClaimReasonEnum::CORRECT_BILLING_INFORMATION())){
            $claimStatusEnum = new ClaimStatusEnum('accepted');
            $applicationFormStatusEnum = new ApplicationFormStatusEnum('claim_accepted');
            $description = $this->translator->trans("automatic_processing");
            $this->claimService->claimDecision($claimStatusEnum, $applicationFormStatusEnum, $description, $claim);
        }

        return $this->handleView(
            $this->view(
                [
                    "html" =>
                        [
                            "application_module_detail" =>
                                $this->render(
                                    "{$templatesPrefix}form/application_claim_thanks.html.twig", [
                                        'detailUrl' => $detailButtonTarget,
                                        'uniqId' => $application->getUniqId(),
                                        'isFromSsoUserCreate' => $isFromSsoUserCreate
                                    ]
                                )->getContent()
                        ]
                ],
                Response::HTTP_OK
            )
        );
    }

    /**
     * Get Claim List
     *
     * IsGranted("ROLE_USER")
     *
     * @Rest\Get("/v1/claims/list", name="get_claim_list")
     *
     * @Rest\QueryParam(name="page", requirements="\d+", default=1)
     * @Rest\QueryParam(name="limit", requirements=@Constraints\Range(min=1, max=100), default=10)
     * @Rest\QueryParam(name="filter", nullable=true)
     * @Rest\QueryParam(name="order", nullable=true)
     *
     * @param int $page
     * @param int $limit
     * @param array|null $filter
     * @param array|null $order
     *
     * @return View
     */
    public function cgetAction(
        int $page,
        int $limit,
        ?array $filter,
        ?array $order
    ) {
        $result = $this->claimRepository->getClaimList($page, $limit, $filter, $order);

        $paginatedCollection = new PaginatedRepresentation(
            new CollectionRepresentation($result['data']),
            'get_claim_list',
            array(),
            $page,
            $limit,
            floor($result['totalRecords'] / $limit),
            'page',
            'limit',
            false,
            $result['totalRecords']
        );

        return View::create($paginatedCollection);
    }

    /**
     * Get Claim
     *
     * IsGranted("ROLE_USER")
     *
     * @Rest\Get("/v1/claims/{id}", name="get_claim")
     *
     * @param Claim $claim
     *
     * @return JsonResponse
     */
    public function getClaim(Claim $claim)
    {
        return $this->json($claim, 200, [], ['groups' => ['claim:read']]);
    }

    /**
     * Claim decision.
     * IsGranted("ROLE_USER")
     *
     * @Rest\Post("/v1/claims/{id}", name="post_claim")
     *
     * @param Request $request
     * @param Claim   $claim
     *
     * @return JsonResponse
     *
     * @Entity("Claim", expr="repository.find(id)")
     *
     */
    public function postClaim(Request $request, Claim $claim)
    {
        $dataFromRequest = $request->request->get('claim_decision');
        $claimStatusEnum = new ClaimStatusEnum($dataFromRequest["status"]);
        $applicationFormStatusEnum = new ApplicationFormStatusEnum('claim_'.$dataFromRequest["status"]);
        $files  = $request->request->get('files');
        $files = (empty($files["0"])) ? [] : explode(',', $files["0"]);
        $description = $dataFromRequest["description"];
        return $this->json($this->claimService->claimDecision($claimStatusEnum, $applicationFormStatusEnum, $description, $claim, $files));
    }

    //TODO: refactor move from controller, same method is in more places
    private function isRequestFromWordpress(Request $request): bool
    {
        // match client
        if (stripos($request->headers->get('X-Client'), 'wordpress') !== false) {
            return true;
        }
        // match user agent
        if (stripos($request->headers->get('User-Agent'), 'wordpress') !== false) {
            return true;
        }

        // match source key in query
        if ($request->get('source') === 'wp') {
            return true;
        }

        return false;
    }
}
