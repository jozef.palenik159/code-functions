<?php

namespace App\Processor;

use App\Events\SendEvents;
use App\Events\SendPaymentReminderEvent;
use App\Repository\ApplicationFormRepository;
use App\Service\NotificationService;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class SendPaymentReminderProcessor
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var ApplicationFormRepository
     */
    private $applicationRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        NotificationService $notificationService,
        ApplicationFormRepository $applicationRepository,
        EventDispatcherInterface $dispatcher,
        LoggerInterface $logger
    ) {
        $this->notificationService = $notificationService;
        $this->applicationRepository = $applicationRepository;
        $this->dispatcher = $dispatcher;
        $this->logger = $logger;
    }

    public function process()
    {
        $allApplicationsForPaymentReminder= $this->applicationRepository->findAllApplicationsForPaymentReminder();
        $this->dispatcher->dispatch(
            new GenericEvent($allApplicationsForPaymentReminder),
            SendEvents::PAYMENT_REMINDER_PROCESS_BATCH_START
        );

        foreach ($allApplicationsForPaymentReminder as $applicationForm) {
            try {
                $this->dispatcher->dispatch(
                    new SendPaymentReminderEvent($applicationForm),
                    SendEvents::PAYMENT_REMINDER_PROCESS_INITIALIZE
                );

                $this->notificationService->sendPaymentReminder($applicationForm, $applicationForm->getApplicantEmail());
                $applicationForm->setPaymentReminderSentAt(new DateTime());
                $this->applicationRepository->save($applicationForm);

            } catch (\Exception $exception) {
                $this->dispatcher->dispatch(
                    new SendPaymentReminderEvent($applicationForm, ['exception' => $exception]),
                    SendEvents::PAYMENT_REMINDER_PROCESS_ERROR
                );
                continue;
            }

            $this->dispatcher->dispatch(
                new SendPaymentReminderEvent($applicationForm),
                SendEvents::PAYMENT_REMINDER_PROCESS_SUCCESS
            );

            $this->dispatcher->dispatch(
                new SendPaymentReminderEvent($applicationForm),
                SendEvents::PAYMENT_REMINDER_PROCESS_COMPLETED
            );
        }

        $this->dispatcher->dispatch(
            new GenericEvent($allApplicationsForPaymentReminder),
            SendEvents::PAYMENT_REMINDER_PROCESS_BATCH_END
        );
    }
}
