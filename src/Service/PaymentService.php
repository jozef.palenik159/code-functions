<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\Payment;
use App\Repository\PaymentRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PaymentService
{
    /**
     * @var BesteronService
     */
    private $besteronService;

    /**
     * @var PaymentRepository
     */
    private $paymentRepository;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(
        BesteronService $besteronService,
        PaymentRepository $paymentRepository,
        UrlGeneratorInterface $urlGenerator
    ) {
        $this->besteronService = $besteronService;
        $this->paymentRepository = $paymentRepository;
        $this->urlGenerator = $urlGenerator;
    }

    public function createPaymentForOrder(Order $order): Payment
    {
        $payment = new Payment();
        $payment->setOrder($order);
        $payment->setAmount($order->getPriceWithVat());
        $payment->setCurrency($order->getCurrency());
        $payment->setVariableSymbol($order->getNumber());

        $this->paymentRepository->save($payment);

        return $payment;
    }

    public function generatePaymentWidgetData(Payment $payment): array
    {
        $returnUrl = $this->urlGenerator->generate('payment_result', [
            'uuid' => $payment->getUuid(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $paymentData = $this->besteronService->generatePaymentDataForWidget(
            $payment->getOrder()->getEmail(),
            $payment->getAmount(),
            $payment->getCurrency(),
            $payment->getVariableSymbol(),
            $returnUrl
        );

        return [
            'paymentData' => $paymentData,
            'scriptUrl' => $this->besteronService->getWidgetScriptUrl(),
            'stylesheetUrl' => $this->besteronService->getWidgetStylesheetUrl(),
        ];
    }

    public function generateOrderReturnUrlFromPayment(Payment $payment): string
    {
        $order = $payment->getOrder();
        $returnUrl = $order->getReturnUrl();
        $returnUrl .= (strpos($returnUrl, '?') === false) ? '?' : '&';

        return $returnUrl . http_build_query([
            'orderUuid' => $order->getUuid(),
            'result' => $payment->getResult(),
        ]);
    }

    public function processPaymentWithResultQuery(Payment $payment, array $query)
    {
        $this->besteronService->verifyResultQuery($query);

        $payment->setResult($query['result']);

        $this->paymentRepository->save($payment);
    }
}
