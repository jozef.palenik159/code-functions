<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderFile;
use Symfony\Component\HttpFoundation\File\File;

class OrderFileService
{
    /**
     * @var string
     */
    private $projectDir;

    public function __construct(string $projectDir)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * @param OrderFile $orderFile
     * @return File
     */
    public function getFile(OrderFile $orderFile)
    {
        return new File(sprintf('%s/%s', $this->projectDir, $orderFile->getPath()));
    }

    /**
     * @param Order $order
     * @param string $type
     * @return File
     */
    public function getFileByOrderAndType(Order $order, string $type): File
    {
        $orderFile = $order->getFiles()->filter(function (OrderFile $orderFile) use ($type) {
            return $orderFile->getDocumentType() === $type;
        })->first();

        return $this->getFile($orderFile);
    }

    /**
     * @param OrderFile $orderFile
     * @return array
     */
    public function fileToNotificationAttachment(OrderFile $orderFile): array
    {
        $file = $this->getFile($orderFile);

        return [
            'filename' => $file->getFilename(),
            'content_type' => $file->getMimeType(),
            'content' => base64_encode(file_get_contents($file->getRealPath())),
        ];
    }
}
