<?php

namespace App\Service;

use App\DTO\PatchOrderInput;
use App\Entity\Order;
use App\Entity\OrderPrice;
use App\Entity\Payment;
use App\Enum\OrderStateEnum;
use App\Enum\PaymentMethodEnum;
use App\Repository\OrderFileRepository;
use App\Repository\OrderRepository;
use App\ValueObject\Number\OrderNumber;
use DateTime;
use DateTimeInterface;
use RuntimeException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class OrderService
{
    /**
     * @var AccountingService
     */
    private $accountingService;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var OrderFileRepository
     */
    private $orderFileRepository;

    /**
     * @var PaymentService
     */
    private $paymentService;

    public function __construct(
        AccountingService $accountingService,
        UrlGeneratorInterface $urlGenerator,
        NotificationService $notificationService,
        OrderRepository $orderRepository,
        OrderFileRepository $orderFileRepository,
        PaymentService $paymentService
    ) {
        $this->accountingService = $accountingService;
        $this->urlGenerator = $urlGenerator;
        $this->notificationService = $notificationService;
        $this->orderRepository = $orderRepository;
        $this->orderFileRepository = $orderFileRepository;
        $this->paymentService = $paymentService;
    }

    public function createOrder(Order $order): Order
    {
        $order->setCurrency('EUR');
        $this->setVatRates($order);

        $price = $this->calculatePriceForOrder($order);

        $order->setPriceWithoutVat($price->getPriceWithoutVat());
        $order->setPriceWithVat($price->getPriceWithVat());

        $this->orderRepository->save($order);

        $number = $this->orderRepository->safeUpdateNumber($order->getId(), function () {
            return $this->generateOrderNumber(new \DateTime());
        });
        $order->setNumber($number);

        return $order;
    }

    public function calculatePriceForOrder(Order $order): OrderPrice
    {
        $order->setCurrency('EUR');
        $this->setVatRates($order);

        $priceWithoutVat = '0';
        foreach ($order->getItems() as $orderItem) {
            $priceWithoutVat = bcadd($priceWithoutVat, $orderItem->getPriceWithoutVat());
        }

        $vatAmount = bcmul($priceWithoutVat, $order->getVatRate());
        $priceWithVat = bcadd($priceWithoutVat, $vatAmount);

        return OrderPrice::fromArray([
            'priceWithoutVat' => $priceWithoutVat,
            'priceWithVat' => $priceWithVat,
            'vatRate' => $order->getVatRate(),
            'vatAmount' => $vatAmount,
            'currency' => $order->getCurrency(),
        ]);
    }

    public function createPaymentForOrder(Order $order): Payment
    {
        if ($order->isPaid()) {
            throw new RuntimeException('Order is already paid');
        }

        $payment = $this->paymentService->createPaymentForOrder($order);

        $order->setState(OrderStateEnum::PENDING_PAYMENT());
        $this->orderRepository->save($order);

        return $payment;
    }

    public function processSuccessfulPayment(Order $order, Payment $payment): void
    {
        if ($order->isPaid()) {
            throw new RuntimeException('Order is already paid');
        }

        if (!$payment->isSuccessful()) {
            throw new RuntimeException(sprintf(
                'Successful payment is required to finish order, result "%s" given',
                $payment->getResult()
            ));
        }

        $order->setState(OrderStateEnum::PAID());
        $order->setPaidAt($payment->getUpdatedAt());
        $this->orderRepository->save($order);

        $this->notificationService->sendPaymentConfirmation(
            $order->getEmail(),
            $order->getNumber(),
            $order->getPaidAt(),
            $order->getItems()->toArray(),
            $payment->getAmount(),
            $payment->getCurrency(),
        );
    }

    public function markAsProcessed(Order $order, DateTime $proccesedAt): void
    {
        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::ONLINE()) && !$order->isPaid()) {
            throw new RuntimeException(sprintf(
                'Online order must be paid before marking as processed. Order state is "%s"',
                $order->getState()
            ));
        }

        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())
            && !$order->getState()->equals(OrderStateEnum::UNPAID())
        ) {
            throw new RuntimeException(sprintf(
                'Cash on delivery order can\'t be paid before marking as processed. Order state is "%s"',
                $order->getState()
            ));
        }

        $order->setState(OrderStateEnum::PROCESSED());
        $order->setProcessedAt($proccesedAt);
        $this->orderRepository->save($order);
    }

    public function cancelOrder(Order $order): void
    {
        if ($order->getState()->equals(OrderStateEnum::CANCELED())) {
            throw new RuntimeException('Order is already canceled');
        }

        // online and paid, but not processed - must have invoice before creating credit note
        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::ONLINE())
            && $order->getState()->equals(OrderStateEnum::PAID())
        ) {
            $this->accountingService->createJournalEntryForPaidOrder($order);
        }

        $orderStateBeforeCancelation = $order->getState();

        // mark order as canceled
        $order->setState(OrderStateEnum::CANCELED());
        $this->orderRepository->save($order);

        // unpaid online order should be only marked as canceled without journal entry
        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::ONLINE())
            && (
                $orderStateBeforeCancelation->equals(OrderStateEnum::UNPAID())
                || $orderStateBeforeCancelation->equals(OrderStateEnum::PENDING_PAYMENT())
            )
        ) {
            return;
        }

        // create credit note
        $this->accountingService->createJournalEntryForCanceledOrder($order);
    }

    public function refundOrder(Order $order, string $iban, ?string $bic)
    {
        if (!$order->getState()->equals(OrderStateEnum::PAID())
            && !$order->getState()->equals(OrderStateEnum::PROCESSED())
        ) {
            throw new RuntimeException('Order must be paid to be refunded');
        }

        // mark order as refunded
        $order->setState(OrderStateEnum::REFUNDED());
        $order->setRefundIban($iban);
        $order->setRefundBic($bic);
        $this->orderRepository->save($order);

        // create credit note
        $this->accountingService->createJournalEntryForRefundedOrder($order);
    }

    public function getOrderFiles(Order $order)
    {
        return $this->orderFileRepository->findByOrder($order);
    }

    public function patchOrder(Order $existingOrder, PatchOrderInput $patchOrderInput): Order
    {
        $orderHasInvoice = $existingOrder->hasInvoiceJournalEntry();

        // create credit note only when there is already an invoice
        if ($orderHasInvoice) {
            $this->accountingService->createCreditNoteJournalEntry($existingOrder);
        }

        $patchedOrder = $this->fillOrderWithPatchInput($existingOrder, $patchOrderInput);

        // create repair invoice only when there is already an invoice
        if ($orderHasInvoice) {
            $this->accountingService->createInvoiceRepairJournalEntry($patchedOrder);
        }

        // save patched order
        $this->orderRepository->save($patchedOrder);

        return $patchedOrder;
    }

    private function generateOrderNumber(DateTimeInterface $dateTime): string
    {
        $orderNumberPattern = (string) OrderNumber::createFromDate($dateTime)->replaceCounterWithSqlWildcard();
        $lastNumber = $this->orderRepository->findLastNumberByPattern($orderNumberPattern);

        // first order this year and month
        if ($lastNumber === null) {
            return (string) OrderNumber::createFromDate($dateTime);
        }

        // increment number of last order
        return (string) OrderNumber::createFromString($lastNumber)->incrementCounter();
    }

    private function setVatRates(Order $order): void
    {
        if ($order->getVatRate() === '') {
            //TODO get current VAT rate
            $order->setVatRate('0.20');
        }

        foreach ($order->getItems() as &$orderItem) {
            if ($orderItem->getVatRate() === '') {
                $orderItem->setVatRate($order->getVatRate());
            }
        }
    }

    private function fillOrderWithPatchInput(Order $existingOrder, PatchOrderInput $patchOrderInput): Order
    {
        $existingOrder->setFirstName($patchOrderInput->firstName);
        $existingOrder->setLastName($patchOrderInput->lastName);

        $existingOrder->setBusinessName($patchOrderInput->businessName ?? '');
        $existingOrder->setCompanyId($patchOrderInput->companyId ?? '');
        $existingOrder->setTaxId($patchOrderInput->taxId ?? '');
        $existingOrder->setVatId($patchOrderInput->vatId ?? '');

        $existingOrder->setCity($patchOrderInput->city);
        $existingOrder->setPostalCode($patchOrderInput->postalCode);
        $existingOrder->setStreet($patchOrderInput->street);
        $existingOrder->setHouseNumber($patchOrderInput->houseNumber);

        return $existingOrder;
    }
}
