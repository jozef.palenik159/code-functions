<?php

namespace App\Service;

use App\Entity\JournalEntry;
use App\Entity\Order;
use App\Entity\Payment;
use App\Enum\DocumentTypeEnum;
use App\Enum\OrderStateEnum;
use App\Enum\PaymentMethodEnum;
use App\Repository\JournalEntryRepository;
use App\Repository\OrderRepository;
use App\ValueObject\Number\CreditNoteCashOnDeliveryNumber;
use App\ValueObject\Number\CreditNoteNumber;
use App\ValueObject\Number\CreditNoteWithRefundNumber;
use App\ValueObject\Number\InvoiceCashOnDeliveryNumber;
use App\ValueObject\Number\InvoiceNumber;
use App\ValueObject\Number\PaymentReceivedInvoiceNumber;
use App\ValueObject\Number\SettlementInvoiceNumber;
use DateTime;
use DateTimeInterface;
use Psr\Log\LoggerInterface;

class AccountingService
{
    /**
     * @var DocumentService
     */
    private $documentService;

    /**
     * @var JournalEntryRepository
     */
    private $journalEntryRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var NotificationService
     */
    private $notificationService;

    public function __construct(
        DocumentService $documentService,
        JournalEntryRepository $journalEntryRepository,
        LoggerInterface $accountingLogger,
        NotificationService $notificationService,
        OrderRepository $orderRepository
    ) {
        $this->documentService = $documentService;
        $this->journalEntryRepository = $journalEntryRepository;
        $this->logger = $accountingLogger;
        $this->notificationService = $notificationService;
        $this->orderRepository = $orderRepository;
    }

    public function getJournalEntry(string $documentNumber): JournalEntry
    {
        return $this->journalEntryRepository->getByDocumentNumber($documentNumber);
    }

    public function batchProcessPaidOrders(DateTimeInterface $date): void
    {
        if ($date->format('Ymd') === (new DateTime('today'))->format('Ymd')) {
            throw new \UnexpectedValueException('You can\'t process orders paid today');
        }

        $paidOrders = $this->orderRepository->findPaidAtDateWithoutJournal($date);

        $this->logger->info('Processing {count} paid orders at {date} for journal', [
            'count' => count($paidOrders),
            'date' => $date->format('Y-m-d'),
        ]);

        foreach ($paidOrders as $order) {
            $this->createJournalEntryForPaidOrder($order);
        }
    }

    public function batchProcessProcessedOrders(DateTimeInterface $date): void
    {
        $processedOrders = $this->orderRepository->findProcessedAtDateWithoutJournal($date);

        $this->logger->info('Processing {count} processed orders at {date} for journal', [
            'count' => count($processedOrders),
            'date' => $date->format('Y-m-d'),
        ]);

        foreach ($processedOrders as $order) {
            $this->createJournalEntryForProcessedOrder($order);
        }
    }

    /**
     * Journal order which was paid sooner than today, but still not processed
     *
     * @param Order $order
     */
    public function createJournalEntryForPaidOrder(Order $order): JournalEntry
    {
        if (!$order->isPaid()) {
            throw new \UnexpectedValueException(
                sprintf(
                    'Order should be paid to create journal entry, order in state "%s" given',
                    $order->getState()
                )
            );
        }

        if ($order->getPaidAt()->format('Ymd') > (new DateTime('yesterday'))->format('Ymd')) {
            throw new \UnexpectedValueException('You can process only orders paid yesterday or sooner');
        }

        if ($this->journalEntryRepository->existsByOrder($order)) {
            throw new \RuntimeException('Journal entry for this order already exists');
        }

        $this->logger->info('Creating journal entry for paid order #{order_id}', ['order_id' => $order->getId()]);

        $paymentReceivedInvoiceNumber = PaymentReceivedInvoiceNumber::createFromOrderNumber($order->getNumber());
        $orderFile = $this->documentService->createPaymentReceivedInvoice(
            $order,
            $order->getSuccessfulPayment(),
            $paymentReceivedInvoiceNumber
        );

        $journalEntry = JournalEntry::createFromOrder($order);
        $journalEntry->setOrderFile($orderFile);
        $journalEntry->setType(DocumentTypeEnum::PAYMENT_RECEIVED_INVOICE());
        $journalEntry->setDocumentNumber($paymentReceivedInvoiceNumber);

        $this->journalEntryRepository->save($journalEntry);
        $this->logger->info('Created journal entry #{journal_entry_id} for order #{order_id}', [
            'journal_entry_id' => $journalEntry->getId(),
            'order_id' => $order->getId(),
        ]);

        return $journalEntry;
    }

    public function createJournalEntryForProcessedOrder(Order $order): ?JournalEntry
    {
        if (!$order->isProcessed()) {
            throw new \UnexpectedValueException(
                sprintf(
                    'Order should be in processed state to create journal entry, "%s" given',
                    $order->getState()
                )
            );
        }

        //TODO check if exists journal entry

        $this->logger->info('Creating journal entry for processed order #{order_id}', ['order_id' => $order->getId()]);

        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())) {
            return $this->createJournalEntryForCashOnDeliveryOrder($order);
        }

        $payment = $order->getSuccessfulPayment();
        if ($order->getPaidAt()->format('Ymd') === $order->getProcessedAt()->format('Ymd')) {
            return $this->createJournalEntryForOrderPaidAndProcessedSameDay($order);
        }

        return $this->createJournalEntryForOrderPaidAndProcessedAnotherDay($order, $payment);
    }

    public function createJournalEntryForCanceledOrder(Order $order): JournalEntry
    {
        if (!$order->getState()->equals(OrderStateEnum::CANCELED())) {
            throw new \UnexpectedValueException(
                sprintf(
                    'Order should be in canceled state to create journal entry, "%s" given',
                    $order->getState()
                )
            );
        }

        $this->logger->info('Creating journal entry for canceled order #{order_id}', ['order_id' => $order->getId()]);

        return $this->createCreditNoteJournalEntry($order);
    }

    public function createJournalEntryForRefundedOrder(Order $order): JournalEntry
    {
        if (!$order->getState()->equals(OrderStateEnum::REFUNDED())) {
            throw new \UnexpectedValueException(
                sprintf(
                    'Order should be in refunded state to create journal entry, "%s" given',
                    $order->getState()
                )
            );
        }

        $this->logger->info('Creating journal entry for refunded order #{order_id}', ['order_id' => $order->getId()]);

        return $this->createCreditNoteJournalEntry($order);
    }

    private function createJournalEntryForOrderPaidAndProcessedSameDay(Order $order): JournalEntry
    {
        if ($order->getPaidAt() === null || $order->getProcessedAt() === null) {
            throw new \UnexpectedValueException('Order must be paid and processed');
        }

        if ($order->getPaidAt()->format('Ymd') !== $order->getProcessedAt()->format('Ymd')) {
            throw new \UnexpectedValueException('Order must be paid and processed at the same day');
        }

        return $this->createInvoiceJournalEntry($order);
    }

    private function createJournalEntryForOrderPaidAndProcessedAnotherDay(Order $order, Payment $payment): JournalEntry
    {
        if ($order->getPaidAt() === null || $order->getProcessedAt() === null) {
            throw new \UnexpectedValueException('Order must be paid and processed');
        }

        if ($order->getPaidAt()->format('Ymd') === $order->getProcessedAt()->format('Ymd')) {
            throw new \UnexpectedValueException('Order must be paid and processed at another day');
        }

        $paymentReceivedJournalEntry = $order->getPaymentReceivedJournalEntry();

        $settlementInvoiceNumber = SettlementInvoiceNumber::createFromDocumentNumber($paymentReceivedJournalEntry->getDocumentNumber());
        $orderFile = $this->documentService->createSettlementInvoice(
            $order,
            $payment,
            $settlementInvoiceNumber
        );

        $journalEntry = JournalEntry::createFromOrder($order);
        $journalEntry->setOrderFile($orderFile);
        $journalEntry->setType(DocumentTypeEnum::SETTLEMENT_INVOICE());
        $journalEntry->setDocumentNumber($settlementInvoiceNumber);
        $journalEntry->setRelatedJournalEntry($paymentReceivedJournalEntry);

        $this->journalEntryRepository->save($journalEntry);

        $this->notificationService->sendSettlementInvoice(
            $order->getEmail(),
            $order->getNumber(),
            $orderFile
        );

        return $journalEntry;
    }

    private function createJournalEntryForCashOnDeliveryOrder(Order $order): JournalEntry
    {
        $invoiceNumber = InvoiceCashOnDeliveryNumber::createFromOrderNumber($order->getNumber());
        $orderFile = $this->documentService->createInvoice($order, $invoiceNumber);

        $journalEntry = JournalEntry::createFromOrder($order);
        $journalEntry->setOrderFile($orderFile);
        $journalEntry->setType(DocumentTypeEnum::INVOICE_CASH_ON_DELIVERY());
        $journalEntry->setDocumentNumber($invoiceNumber);

        $this->journalEntryRepository->save($journalEntry);

        return $journalEntry;
    }

    public function createInvoiceRepairJournalEntry(Order $order): JournalEntry
    {
        if (!$order->hasInvoiceJournalEntry()) {
            throw new \RuntimeException('Can\'t create repair invoice for order without invoice');
        }

        return $this->createInvoiceJournalEntry($order);
    }

    private function createInvoiceJournalEntry(Order $order): JournalEntry
    {
        // set defaults for first invoice based on payment method
        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())) {
            $invoiceNumber = InvoiceCashOnDeliveryNumber::createFromOrderNumber($order->getNumber());
            $documentType = DocumentTypeEnum::INVOICE_CASH_ON_DELIVERY();
        } else {
            $invoiceNumber = InvoiceNumber::createFromOrderNumber($order->getNumber());
            $documentType = DocumentTypeEnum::INVOICE();
        }

        // check for previous invoices
        $lastInvoiceJournalEntry = $order->getLastInvoiceJournalEntry();

        // invoice is not first, change invoice number to incremented from last one
        if ($lastInvoiceJournalEntry !== null) {
            if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())) {
                $invoiceNumber = InvoiceCashOnDeliveryNumber::createFromDocumentNumber($lastInvoiceJournalEntry->getDocumentNumber())
                    ->incrementTypeSection();
            } else {
                $invoiceNumber = InvoiceNumber::createFromDocumentNumber($lastInvoiceJournalEntry->getDocumentNumber())
                    ->incrementTypeSection();
            }
        }

        $orderFile = $this->documentService->createInvoice($order, $invoiceNumber);

        $journalEntry = JournalEntry::createFromOrder($order);
        $journalEntry->setOrderFile($orderFile);
        $journalEntry->setType($documentType);
        $journalEntry->setDocumentNumber($invoiceNumber);

        $this->journalEntryRepository->save($journalEntry);

        // send invoice notification only to online payment
        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::ONLINE())) {
            $this->notificationService->sendInvoice($order->getEmail(), $order->getNumber(), $orderFile);
        }

        return $journalEntry;
    }

    public function createCreditNoteJournalEntry(Order $order): JournalEntry
    {
        $invoiceJournalEntry = $order->getLastInvoiceJournalEntry();
        if ($invoiceJournalEntry === null) {
            throw new \RuntimeException(sprintf('Order #%s is missing invoice journal entry', $order->getNumber()));
        }

        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())) {
            $creditNoteNumber = CreditNoteCashOnDeliveryNumber::createFromDocumentNumber($invoiceJournalEntry->getDocumentNumber());
            $documentType = DocumentTypeEnum::CREDIT_NOTE_CASH_ON_DELIVERY();
        } else {
            $creditNoteNumber = CreditNoteNumber::createFromDocumentNumber($invoiceJournalEntry->getDocumentNumber());
            $documentType = DocumentTypeEnum::CREDIT_NOTE();
        }

        if ($order->getState()->equals(OrderStateEnum::REFUNDED())) {
            $creditNoteNumber = CreditNoteWithRefundNumber::createFromDocumentNumber($invoiceJournalEntry->getDocumentNumber());
            $documentType = DocumentTypeEnum::CREDIT_NOTE_WITH_REFUND();
        }

        $journalEntry = JournalEntry::createFromOrder($order);
        $journalEntry->setType($documentType);
        $journalEntry->setDocumentNumber($creditNoteNumber);
        $journalEntry->setRelatedJournalEntry($invoiceJournalEntry);

        $orderFile = $this->documentService->createCreditNote(
            $order,
            $creditNoteNumber,
            $invoiceJournalEntry->getDocumentNumber()
        );
        $journalEntry->setOrderFile($orderFile);

        $this->journalEntryRepository->save($journalEntry);

        // send credit note notification only to online payment
        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::ONLINE())) {
            $this->notificationService->sendCreditNote(
                $order->getEmail(),
                $creditNoteNumber,
                $order->getNumber(),
                $order->getFullName(),
                $order->getFirstName(),
                $order->getLastName(),
                $orderFile,
                $order->getRefundIban(),
                $order->getRefundBic()
            );
        }

        return $journalEntry;
    }
}
