<?php

namespace App\Service;

use App\Entity\JournalEntry;
use App\Enum\DocumentTypeEnum;
use App\Repository\JournalEntryRepository;
use App\Soap\SapClientFactory;
use App\Soap\Type\ZfiSWebDocInData;
use App\ValueObject\Number\DocumentNumber;
use DateTime;
use Psr\Log\LoggerInterface;

class SapService
{
    /**
     * @var SapClientFactory
     */
    private $clientFactory;

    /**
     * @var JournalEntryRepository
     */
    private $journalEntryRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var OrderFileService
     */
    private $orderFileService;

    public function __construct(
        JournalEntryRepository $journalEntryRepository,
        OrderFileService $orderFileService,
        SapClientFactory $sapClientFactory,
        LoggerInterface $sapLogger
    ) {
        $this->journalEntryRepository = $journalEntryRepository;
        $this->orderFileService = $orderFileService;
        $this->clientFactory = $sapClientFactory;
        $this->logger = $sapLogger;
    }

    public function sendJournalEntryDocument(JournalEntry $journalEntry)
    {
        $webDocInData = $this->createWebDocInDataFromJournalEntry($journalEntry);

        $response = $this->clientFactory->createWebDocumentClient()->send($webDocInData);

        $this->logger->debug('Send journal entry document response', [
            'response' => $response,
        ]);

        if ($response) {
            $journalEntry->setSapDocumentCreatedAt(new DateTime());
            $this->journalEntryRepository->save($journalEntry);
        }

        return true;
    }

    public function checkJournalEntryByDocumentNumber(string $documentNumber): bool
    {
        $journalEntry = $this->journalEntryRepository->getByDocumentNumber($documentNumber);
        return $this->checkJournalEntryDocument($journalEntry);
    }

    public function checkJournalEntryDocument(JournalEntry $journalEntry): bool
    {
        $documentNumber = (string) $journalEntry->getDocumentNumber();

        $this->logger->info('Checking document #{document_number} status', [
            'document_number' => $documentNumber,
        ]);

        $result = $this->clientFactory->createWebCheckClient()->check($documentNumber);
        $this->logger->debug('Document #{document_number} status"', [
            'document_number' => $documentNumber,
            'result' => $result,
        ]);

        // I002 doklad XY je zaúčtovaný
        $isAccounted = (strpos($result->getFreeText(), 'I002') === 0);
        if ($isAccounted && $journalEntry->getSapDocumentAccountedAt() === null) {
            $journalEntry->setSapDocumentAccountedAt(new DateTime());
            $this->journalEntryRepository->save($journalEntry);
        }

        return $isAccounted;
    }

    public function sendJournalEntryDocumentAttachment(JournalEntry $journalEntry)
    {
        $orderFile = $journalEntry->getOrderFile();
        $documentNumber = (string) $journalEntry->getDocumentNumber();

        $this->logger->info('Sending attachment of document #{document_number}', [
            'document_number' => $documentNumber,
        ]);

        if ($journalEntry->getSapAttachmentUploadedAt() !== null) {
            $this->logger->warning('Document #{document_number} attachment was already uploaded at {uploaded_at}', [
                'document_number' => $documentNumber,
                'uploaded_at' => $journalEntry->getSapAttachmentUploadedAt(),
            ]);
        }

        $file = $this->orderFileService->getFile($orderFile);
        $response = $this->clientFactory->createWebAttachmentClient()->upload(
            $journalEntry->getDocumentNumber(),
            file_get_contents($file->getRealPath())
        );

        $this->logger->debug('Sent document #{document_number} attachment response', [
            'document_number' => $documentNumber,
            'response' => $response,
        ]);

        if ($response) {
            $journalEntry->setSapAttachmentUploadedAt(new DateTime());
            $this->journalEntryRepository->save($journalEntry);
        }

        return true;
    }

    /**
     * @return JournalEntry[]
     */
    public function getJournalEntriesNotExistingInSap(): array
    {
        return $this->journalEntryRepository->findBySapCreatedAt(null);
    }

    public function getJournalEntriesWithNotUploadedAttachments()
    {
        return $this->journalEntryRepository->findBySapAttachmentUploadedAt(null);
    }

    private function createWebDocInDataFromJournalEntry(JournalEntry $journalEntry)
    {
        $order = $journalEntry->getOrder();

        $paymentDate = $order->getPaidAt();
        $deliveryDate = $order->getProcessedAt();
        $documentGeneratedDate = $journalEntry->getCreatedAt();

        // upravy datumov dohodnute telefonicky a mailom 16.06.2020 16:29

        if ($journalEntry->getType()->equals(DocumentTypeEnum::INVOICE_CASH_ON_DELIVERY())) {
            $paymentDate = $documentGeneratedDate;
        }

        if ($journalEntry->getType()->equals(DocumentTypeEnum::PAYMENT_RECEIVED_INVOICE())) {
            $deliveryDate = $documentGeneratedDate;
        }

        if ($journalEntry->getType()->equals(DocumentTypeEnum::SETTLEMENT_INVOICE())) {
            $paymentDate = $documentGeneratedDate;
        }

        if ($journalEntry->getType()->equals(DocumentTypeEnum::CREDIT_NOTE_CASH_ON_DELIVERY())) {
            $paymentDate = $documentGeneratedDate;
            $deliveryDate = $documentGeneratedDate;
        }

        if ($journalEntry->getType()->equals(DocumentTypeEnum::CREDIT_NOTE_WITH_REFUND())) {
            $paymentDate = $documentGeneratedDate;
            $deliveryDate = $documentGeneratedDate;
        }

        $webDocInData = new ZfiSWebDocInData(
            $journalEntry->getDocumentNumber(),
            '01', // TODO pri pridavani novej sluzby vymysliet ako oznacovat tieto typy uz pri vytvoreni objednavky
            $paymentDate,
            $deliveryDate,
            $this->getTypeOfBusinessCaseFromDocumentNumber($journalEntry->getDocumentNumber()),
            $journalEntry->getName(),
            $journalEntry->getStreet(),
            $journalEntry->getCity(),
            $journalEntry->getPostalCode(),
            $journalEntry->getCountry(),
            $journalEntry->getVatId(),
            $journalEntry->getTaxId(),
            $journalEntry->getCompanyId(),
            $this->formatPrice($order->getPriceWithVat()),
            $this->formatPrice($order->getVatAmount())
        );

        if ($journalEntry->getType()->isCreditNote()) {
            $webDocInData->setFakturaDobropis($journalEntry->getRelatedJournalEntry()->getDocumentNumber());

            $webDocInData->setIban($order->getRefundIban());
            $webDocInData->setSwiftCode($order->getRefundBic());
        }

        return $webDocInData;
    }

    private function formatPrice(string $price): string
    {
        return number_format((float) $price, 2, ',', '');
    }

    private function getTypeOfBusinessCaseFromDocumentNumber(DocumentNumber $documentNumber): string
    {
        $type = $documentNumber->getType();

        // opravna faktura opravnej faktury musi byt stale 11
        if ($type >= 12 && $type < 15) {
            return '11';
        }

        // opravna faktura dobierky musi byt stale 15
        if ($type >= 16 && $type < 20) {
            return '15';
        }

        return $type;
    }
}
