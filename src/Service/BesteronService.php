<?php

namespace App\Service;

use Symfony\Component\Intl\Currencies;
use UnexpectedValueException;

class BesteronService
{
    private const GATEWAY_URL_PRODUCTION = 'https://payments.besteron.com/pay-request/';
    private const GATEWAY_URL_VIRTUAL = 'https://client.besteron.com/public/virtual-payment/request/';

    private const WIDGET_SCRIPT_URL_PRODUCTION = 'https://client.besteron.com/inline/besteronInline.js';
    private const WIDGET_SCRIPT_URL_VIRTUAL = 'https://client.besteron.com/inline/besteronInlineVirtual.js';
    private const WIDGET_STYLESHEET_URL_PRODUCTION = 'https://client.besteron.com/inline/css/widget.css';

    /**
     * @var string
     */
    private $cid;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var bool
     */
    private $virtual;

    public function __construct(string $cid, string $key, string $apiKey, bool $virtual = true)
    {
        $this->cid = $cid;
        $this->key = $key;
        $this->apiKey = $apiKey;
        $this->virtual = $virtual;
    }

    public function getGatewayUrl(): string
    {
        return $this->virtual ? self::GATEWAY_URL_VIRTUAL : self::GATEWAY_URL_PRODUCTION;
    }

    public function getWidgetScriptUrl(): string
    {
        return $this->virtual ? self::WIDGET_SCRIPT_URL_VIRTUAL : self::WIDGET_SCRIPT_URL_PRODUCTION;
    }

    public function getWidgetStylesheetUrl(): string
    {
        return self::WIDGET_STYLESHEET_URL_PRODUCTION;
    }

    public function generatePaymentRequestUrl(
        string $type,
        string $email,
        float $amount,
        string $currency,
        string $variableSymbol,
        string $returnUrl
    ) {
        $data = [
            'lang' => 'SK',
            'cid' => $this->cid,
            'type' => $type,
            'amnt' => number_format($amount, 2, '.', ''),
            'curr' => Currencies::getNumericCode($currency),
            'vs' => $variableSymbol,
            'ru' => $returnUrl,
            'email' => $email,
        ];

        $sign = $this->generateSign(sprintf(
            '%s%s%s%s%s%s',
            $data['cid'],
            $data['type'],
            $data['amnt'],
            $data['curr'],
            $data['vs'],
            $data['ru']
        ));
        $data['sign'] = $sign;

        return $this->getGatewayUrl() . '?' . http_build_query($data);
    }

    public function generatePaymentDataForWidget(
        string $email,
        float $amount,
        string $currency,
        string $variableSymbol,
        string $returnUrl
    ): array {
        $data = [
            'cid' => $this->cid,
            'amnt' => number_format($amount, 2, '.', ''),
            'vs' => $variableSymbol,
            'curr' => Currencies::getNumericCode($currency),
            'ru' => $returnUrl,
            'language' => 'SK',
            'paymentmethod' => 'CARDANDBUTTON',
            'email' => $email,
        ];

        $sign = $this->generateSign(sprintf(
            '%s%s%s%s%s%s',
            $data['cid'],
            $data['paymentmethod'],
            $data['amnt'],
            $data['curr'],
            $data['vs'],
            $data['ru']
        ));
        $data['sign'] = $sign;

        return $data;
    }

    /**
     * @param array $query
     * @return bool
     * @throws UnexpectedValueException
     */
    public function verifyResultQuery(array $query): bool
    {
        $required = ['type', 'amnt', 'curr', 'vs', 'result'];
        if (count(array_intersect_key(array_flip($required), $query)) !== count($required)) {
            throw new UnexpectedValueException(sprintf(
                'Query is missing some of it\'s required fields, got "%s"',
                implode('", "', array_keys($query))
            ));
        }

        $sign = $this->generateSign(sprintf(
            '%s%s%s%s%s%s',
            $this->cid,
            $query['type'],
            $query['amnt'],
            $query['curr'],
            $query['vs'],
            $query['result']
        ));

        if ($sign !== $query['sign']) {
            throw new UnexpectedValueException('Invalid payment gateway sign');
        }

        return true;
    }

    private function generateSign(string $string): string
    {
        $key = pack('H*', $this->key);

        $hash = sha1($string);
        $hash16 = substr($hash, 0, 32); // first 16 bytes
        $cipherMethod = 'aes-256-ecb';
        $ivSize = openssl_cipher_iv_length($cipherMethod);
        $iv = openssl_random_pseudo_bytes($ivSize);
        $result = openssl_encrypt(
            pack('H*', $hash16),
            $cipherMethod,
            $key,
            OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING,
            $iv
        );

        return strtoupper(bin2hex($result));
    }
}
