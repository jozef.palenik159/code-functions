<?php

namespace App\Service;

use App\Entity\OrderFile;
use App\Entity\OrderItem;
use DateTimeInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class NotificationService
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderFileService
     */
    private $orderFileService;

    /**
     * @var PdfService
     */
    private $pdfService;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    public function __construct(
        HttpClientInterface $notificationModuleApiClient,
        LoggerInterface $notificationLogger,
        OrderFileService $orderFileService,
        PdfService $pdfService,
        UrlGeneratorInterface $router
    ) {
        $this->client = $notificationModuleApiClient;
        $this->logger = $notificationLogger;
        $this->orderFileService = $orderFileService;
        $this->pdfService = $pdfService;
        $this->router = $router;
    }

    /**
     * @param string $email
     * @param string $orderNumber
     * @param DateTimeInterface $paidAt
     * @param OrderItem[] $orderItems
     * @param float $paymentAmount
     * @param string $paymentCurrency
     * @return bool
     */
    public function sendPaymentConfirmation(
        string $email,
        string $orderNumber,
        DateTimeInterface $paidAt,
        array $orderItems,
        float $paymentAmount,
        string $paymentCurrency
    ): bool {
        $response = $this->client->request('POST', 'send', [
            'json' => [
                'key' => 'payment-confirmation',
                'recipient' => $email,
                'fields' => [
                    'order' => [
                        'number' => $orderNumber,
                        'paid_at' => $paidAt->format('Y-m-d H:i:s'),
                        'items' => array_map(function (OrderItem $orderItem) {
                            return $orderItem->getName();
                        }, $orderItems),
                    ],
                    'payment' => [
                        'amount' => $paymentAmount,
                        'currency' => $paymentCurrency,
                    ],
                    'return_policy_url' => $this->absoluteUrl('web_portal_return_policy'),
                    'terms_and_conditions_url' => $this->absoluteUrl('web_portal_terms_and_conditions'),
                ],
                'attachments' => [
                    $this->fileToAttachment($this->pdfService->getTermsAndConditionsFile()),
                ],
            ]
        ]);
        $this->logger->debug('Send payment confirmation notification', $response->getInfo());

        return ($response->getStatusCode() === 200);
    }

    public function sendInvoice(
        string $email,
        string $orderNumber,
        OrderFile $orderFile
    ): bool {
        $attachment = $this->orderFileService->fileToNotificationAttachment($orderFile);

        $response = $this->client->request('POST', 'send', [
            'json' => [
                'key' => 'invoice',
                'recipient' => $email,
                'fields' => [
                    'order' => [
                        'number' => $orderNumber,
                    ],
                ],
                'attachments' => [
                    $attachment,
                ],
            ]
        ]);
        $this->logger->debug('Send invoice notification', $response->getInfo());

        return ($response->getStatusCode() === 200);
    }

    public function sendSettlementInvoice(
        string $email,
        string $orderNumber,
        OrderFile $orderFile
    ): bool {
        $attachment = $this->orderFileService->fileToNotificationAttachment($orderFile);

        $response = $this->client->request('POST', 'send', [
            'json' => [
                'key' => 'invoice',
                'recipient' => $email,
                'fields' => [
                    'order' => [
                        'number' => $orderNumber,
                    ],
                ],
                'attachments' => [
                    $attachment,
                ],
            ]
        ]);
        $this->logger->debug('Send settlement invoice notification', $response->getInfo());

        return ($response->getStatusCode() === 200);
    }

    public function sendCreditNote(
        string $email,
        string $creditNoteNumber,
        string $orderNumber,
        string $fullName,
        string $firstName,
        string $lastName,
        OrderFile $orderFile,
        ?string $refundIban,
        ?string $refundBic
    ): bool {
        $attachment = $this->orderFileService->fileToNotificationAttachment($orderFile);

        $response = $this->client->request('POST', 'send', [
            'json' => [
                'key' => 'credit-note',
                'recipient' => $email,
                'fields' => [
                    'credit_note_number' => $creditNoteNumber,
                    'user' => [
                        'full_name' => $fullName,
                        'first_name' => $firstName,
                        'last_name' => $lastName,
                    ],
                    'order' => [
                        'refund_iban' => $refundIban,
                        'refund_bic' => $refundBic,
                        'number' => $orderNumber,
                    ],
                ],
                'attachments' => [
                    $attachment,
                ],
            ]
        ]);
        $this->logger->debug('Send credit note notification', $response->getInfo());

        return ($response->getStatusCode() === 200);
    }

    private function absoluteUrl(string $route): string
    {
        return $this->router->generate($route, [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    private function fileToAttachment(File $file): array
    {
        return [
            'filename' => $file->getFilename(),
            'content_type' => $file->getMimeType(),
            'content' => base64_encode(file_get_contents($file->getRealPath())),
        ];
    }
}
