<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\OrderFile;
use App\Entity\Payment;
use App\Enum\DocumentTypeEnum;
use App\Enum\PaymentMethodEnum;
use App\Repository\OrderFileRepository;
use App\ValueObject\Number\AbstractCreditNoteNumber;
use App\ValueObject\Number\AbstractInvoiceNumber;
use App\ValueObject\Number\CreditNoteWithRefundNumber;
use App\ValueObject\Number\DocumentNumber;
use App\ValueObject\Number\InvoiceCashOnDeliveryNumber;
use App\ValueObject\Number\InvoiceNumber;
use App\ValueObject\Number\PaymentReceivedInvoiceNumber;
use App\ValueObject\Number\SettlementInvoiceNumber;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocumentService
{
    private const SUPPLIER = [
        'name' => 'SPP - distribúcia, a.s.',
        'street' => 'Mlynské nivy 44/b',
        'postalCode' => '825 11',
        'city' => 'Bratislava',
        'country' => 'Slovenská republika',
        'companyId' => '35 910 739',
        'taxId' => '2021931109',
        'vatId' => 'SK2021931109',
        'bankAccounts' => [
            ['name' => 'Slovenská sporiteľňa', 'number' => 'SK33 0900 0000 0006 3064 4230'],
        ],
    ];

    /**
     * @var OrderFileRepository
     */
    private $orderFileRepository;

    /**
     * @var PdfService
     */
    private $pdfService;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(
        OrderFileRepository $orderFileRepository,
        PdfService $pdfService,
        TranslatorInterface $translator
    ) {
        $this->orderFileRepository = $orderFileRepository;
        $this->pdfService = $pdfService;
        $this->translator = $translator;
    }

    /**
     * @param Order $order
     * @param InvoiceNumber|InvoiceCashOnDeliveryNumber $invoiceNumber
     * @return OrderFile
     */
    public function createInvoice(Order $order, $invoiceNumber): OrderFile
    {
        if (!($invoiceNumber instanceof InvoiceNumber)
            && !($invoiceNumber instanceof InvoiceCashOnDeliveryNumber)
        ) {
            throw new \InvalidArgumentException(sprintf(
                'Invoice number must be instance of InvoiceNumber or InvoiceCashOnDeliveryNumber, "%s" given.',
                get_class($invoiceNumber)
            ));
        }

        $items = [];
        foreach ($order->getItems() as $orderItem) {
            $items[] = [
                'name' => $orderItem->getName(),
                'priceWithoutVat' => $orderItem->getPriceWithoutVat(),
                'priceWithVat' => $orderItem->getPriceWithVat(),
                'vatRate' => bcmul($orderItem->getVatRate(), '100'),
                'vatAmount' => $orderItem->getVatAmount(),
            ];
        }

        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())) {
            $documentType = DocumentTypeEnum::INVOICE_CASH_ON_DELIVERY();
            $variableSymbol = (string) $invoiceNumber; // requested by Tothova
            $dueDate = (clone $order->getProcessedAt())->modify('+18 days');
        } else {
            $documentType = DocumentTypeEnum::INVOICE();
            $variableSymbol = (string) $order->getNumber();
            $dueDate = $order->getPaidAt();
        }

        $issueDate = $order->getProcessedAt();
        $deliveryDate = $order->getProcessedAt();

        $orderFile = new OrderFile();
        $orderFile->setOrder($order);
        $orderFile->setDocumentType($documentType);

        $filePath = $this->pdfService->generateInvoice($order->getStoragePath(), [
            'supplier' => self::SUPPLIER,
            'customer' => [
                'name' => $order->getFullName(),
                'street' => $order->getStreet(),
                'houseNumber' => $order->getHouseNumber(),
                'postalCode' => $order->getPostalCode(),
                'city' => $order->getCity(),
                'countryName' => $order->getCountryName(),
                'companyId' => $order->getCompanyId(),
                'taxId' => $order->getTaxId(),
                'vatId' => $order->getVatId(),
            ],
            'headerType' => 'invoice',
            'footerType' => null,
            'documentNumber' => (string) $invoiceNumber,
            'items' => $items,
            // payment info
            'orderNumber' => (string) $order->getNumber(),
            'variableSymbol' => $variableSymbol,
            'constantSymbol' => '0308',
            'orderDate' => $order->getCreatedAt()->format('Y-m-d'),
            'dueDate' => $dueDate->format('Y-m-d'),
            'issueDate' => $issueDate->format('Y-m-d'),
            'deliveryDate' => $deliveryDate->format('Y-m-d'),
            'currency' => $order->getCurrency(),
            'paymentMethod' => $this->translator->trans((string) $order->getPaymentMethod()),
            // summary
            'summaryPriceWithoutVat' => $order->getPriceWithoutVat(),
            'summaryPriceWithVat' => $order->getPriceWithVat(),
            'summaryVatAmount' => $order->getVatAmount(),
            'summaryVatRate' => $order->getVatRatePercentage(),
            // total
            'totalPriceWithVat' => $order->getPriceWithVat(),
        ]);

        $orderFile->setPath($filePath);
        $this->orderFileRepository->save($orderFile);

        return $orderFile;
    }

    public function createPaymentReceivedInvoice(
        Order $order,
        Payment $payment,
        PaymentReceivedInvoiceNumber $paymentReceivedInvoiceNumber
    ): OrderFile {
        $items = [];
        foreach ($order->getItems() as $orderItem) {
            $items[] = [
                'name' => $orderItem->getName(),
                'priceWithoutVat' => $orderItem->getPriceWithoutVat(),
                'priceWithVat' => $orderItem->getPriceWithVat(),
                'vatRate' => bcmul($orderItem->getVatRate(), '100'),
                'vatAmount' => $orderItem->getVatAmount(),
            ];
        }

        $issueDate = new \DateTime();
        $dueDate = $order->getPaidAt();
        $paymentDate = $order->getPaidAt();

        $orderFile = new OrderFile();
        $orderFile->setOrder($order);
        $orderFile->setDocumentType(DocumentTypeEnum::PAYMENT_RECEIVED_INVOICE());

        $filePath = $this->pdfService->generateInvoicePaymentReceived($order->getStoragePath(), [
            'supplier' => self::SUPPLIER,
            'customer' => [
                'name' => $order->getFullName(),
                'street' => $order->getStreet(),
                'houseNumber' => $order->getHouseNumber(),
                'postalCode' => $order->getPostalCode(),
                'city' => $order->getCity(),
                'country' => $order->getCountryName(),
                'companyId' => $order->getCompanyId(),
                'taxId' => $order->getTaxId(),
                'vatId' => $order->getVatId(),
            ],
            'headerType' => 'invoice',
            'footerType' => null,
            'documentNumber' => (string) $paymentReceivedInvoiceNumber,
            'items' => $items,
            // payment info
            'orderNumber' => (string) $order->getNumber(),
            'variableSymbol' => $payment->getVariableSymbol(),
            'constantSymbol' => '0308',
            'orderDate' => $order->getCreatedAt()->format('Y-m-d'),
            'dueDate' => $dueDate->format('Y-m-d'),
            'issueDate' => $issueDate->format('Y-m-d'),
            'paymentDate' => $paymentDate->format('Y-m-d'),
            'currency' => $order->getCurrency(),
            'paymentMethod' => $this->translator->trans((string) $order->getPaymentMethod()),
            // payment received
            'paidAmount' => $payment->getAmount(),
        ]);

        $orderFile->setPath($filePath);
        $this->orderFileRepository->save($orderFile);

        return $orderFile;
    }

    /**
     * @param Order $order
     * @param AbstractCreditNoteNumber $creditNoteNumber
     * @param DocumentNumber $creditedInvoiceDocumentNumber
     * @return OrderFile
     */
    public function createCreditNote(
        Order $order,
        AbstractCreditNoteNumber $creditNoteNumber,
        DocumentNumber $creditedInvoiceDocumentNumber
    ): OrderFile {
        if (!($creditNoteNumber instanceof AbstractCreditNoteNumber)) {
            throw new \InvalidArgumentException(sprintf(
                'Credit note number must be instance of CreditNoteNumber or CreditNoteCashOnDeliveryNumber, "%s" given.',
                get_class($creditNoteNumber)
            ));
        }

        $items = [];
        foreach ($order->getItems() as $orderItem) {
            $items[] = [
                'name' => $orderItem->getName(),
                'priceWithoutVat' => $orderItem->getPriceWithoutVat(),
                'priceWithVat' => $orderItem->getPriceWithVat(),
                'vatRate' => bcmul($orderItem->getVatRate(), '100'),
                'vatAmount' => $orderItem->getVatAmount(),
            ];
        }

        if ($order->getPaymentMethod()->equals(PaymentMethodEnum::CASH_ON_DELIVERY())) {
            $documentType = DocumentTypeEnum::CREDIT_NOTE_CASH_ON_DELIVERY();
        } else {
            $documentType = DocumentTypeEnum::CREDIT_NOTE();
        }

        if ($creditNoteNumber instanceof CreditNoteWithRefundNumber) {
            $paymentMethod = $this->translator->trans('payment_method.bank_transfer', [], 'document');
            $dueDate = new \DateTime('+30 days');
        } else {
            $paymentMethod = $this->translator->trans('payment_method.without_payment', [], 'document');
            $dueDate = null;
        }

        $issueDate = new \DateTime();
        $deliveryDate = $order->getProcessedAt();

        $orderFile = new OrderFile();
        $orderFile->setOrder($order);
        $orderFile->setDocumentType($documentType);

        $filePath = $this->pdfService->generateCreditNote($order->getStoragePath(), [
            'supplier' => self::SUPPLIER,
            'customer' => [
                'name' => $order->getFullName(),
                'street' => $order->getStreet(),
                'houseNumber' => $order->getHouseNumber(),
                'postalCode' => $order->getPostalCode(),
                'city' => $order->getCity(),
                'countryName' => $order->getCountryName(),
                'companyId' => $order->getCompanyId(),
                'taxId' => $order->getTaxId(),
                'vatId' => $order->getVatId(),
            ],
            'headerType' => 'invoice',
            'footerType' => null,
            'documentNumber' => (string) $creditNoteNumber,
            'items' => $items,
            // payment info
            'orderNumber' => (string) $order->getNumber(),
            'creditedInvoiceNumber' => (string) $creditedInvoiceDocumentNumber,
            'variableSymbol' => (string) $order->getNumber(),
            'constantSymbol' => '0308',
            'orderDate' => $order->getCreatedAt()->format('Y-m-d'),
            'dueDate' => $dueDate ? $dueDate->format('Y-m-d') : null,
            'issueDate' => $issueDate->format('Y-m-d'),
            'deliveryDate' => $deliveryDate->format('Y-m-d'),
            'currency' => $order->getCurrency(),
            'paymentMethod' => $paymentMethod,
            'priceWithoutVat' => -1 * $order->getPriceWithoutVat(),
            'priceWithVat' => -1 * $order->getPriceWithVat(),
            'vatRate' => $order->getVatRate() * 100,
            'vat' => 20,
            'vatRatePercentage' => $order->getVatRatePercentage(),
            'vatAmount' => $order->getVatAmount(),
            'totalPriceWithVatToPay' => $order->getPriceWithVat(),
            'summaryVat' => $order->getVatRatePercentage(),
            'sumaryTotalPriceVat' => $order->getVatAmount(),
            'totalPricePriceWithoutVat' => $order->getPriceWithoutVat(),
            'summaryTotalPriceWithVat' => $order->getPriceWithVat(),
        ]);

        $orderFile->setPath($filePath);
        $this->orderFileRepository->save($orderFile);

        return $orderFile;
    }

    public function createSettlementInvoice(
        Order $order,
        Payment $payment,
        SettlementInvoiceNumber $settlementInvoiceNumber
    ): OrderFile {
        $items = [];
        foreach ($order->getItems() as $orderItem) {
            $items[] = [
                'name' => $orderItem->getName(),
                'priceWithoutVat' => $orderItem->getPriceWithoutVat(),
                'priceWithVat' => $orderItem->getPriceWithVat(),
                'vatRate' => bcmul($orderItem->getVatRate(), '100'),
                'vatAmount' => $orderItem->getVatAmount(),
            ];
        }

        $issueDate = new \DateTime();
        $deliveryDate = $order->getProcessedAt();
        $dueDate = $order->getPaidAt();
        $paymentDate = $order->getPaidAt();

        $orderFile = new OrderFile();
        $orderFile->setOrder($order);
        $orderFile->setDocumentType(DocumentTypeEnum::SETTLEMENT_INVOICE());

        $filePath = $this->pdfService->generateSettlementInvoice($order->getStoragePath(), [
            'supplier' => self::SUPPLIER,
            'customer' => [
                'name' => $order->getFullName(),
                'street' => $order->getStreet(),
                'houseNumber' => $order->getHouseNumber(),
                'postalCode' => $order->getPostalCode(),
                'city' => $order->getCity(),
                'countryName' => $order->getCountryName(),
                'companyId' => $order->getCompanyId(),
                'taxId' => $order->getTaxId(),
                'vatId' => $order->getVatId(),
            ],
            'headerType' => 'invoice',
            'footerType' => null,
            'documentNumber' => (string) $settlementInvoiceNumber,
            'items' => $items,
            // payment info
            'orderNumber' => (string) $order->getNumber(),
            'variableSymbol' => $payment->getVariableSymbol(),
            'constantSymbol' => '0308',
            'orderDate' => $order->getCreatedAt()->format('Y-m-d'),
            'dueDate' => $dueDate->format('Y-m-d'),
            'issueDate' => $issueDate->format('Y-m-d'),
            'paymentDate' => $paymentDate->format('Y-m-d'),
            'deliveryDate' => $deliveryDate->format('Y-m-d'),
            'currency' => $order->getCurrency(),
            'paymentMethod' => $this->translator->trans((string) $order->getPaymentMethod()),
            // summary
            'summaryPriceWithoutVat' => $order->getPriceWithoutVat(),
            'summaryPriceWithVat' => $order->getPriceWithVat(),
            'summaryVatAmount' => $order->getVatAmount(),
            'summaryVatRate' => $order->getVatRatePercentage(),
            // payment received
            'paidAmount' => $payment->getAmount(),
            // total
            'totalPriceWithVat' => 0,
        ]);

        $orderFile->setPath($filePath);
        $this->orderFileRepository->save($orderFile);

        return $orderFile;
    }
}
