<?php

namespace App\Command;

use App\Command\Command;
use App\Events\SendEvents;
use App\Events\SendPaymentReminderEvent;
use App\Processor\SendPaymentReminderProcessor;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class SendPaymentReminderCommand extends Command
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var SendPaymentReminderProcessor
     */
    private $sendPaymentReminderProcessor;

    /**
     * @var LoggerInterface $logger
     */
    protected $logger;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        SendPaymentReminderProcessor $sendPaymentReminderProcessor,
        LoggerInterface $logger
        )
    {
        $this->dispatcher = $dispatcher;
        $this->sendPaymentReminderProcessor = $sendPaymentReminderProcessor;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:payment-reminder')
            ->setDescription('When application is in status  payment_wait or payment_fail send notification  of a re-payment after 30 minutes of create application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Processing send payment reminder...');

        $counter = [
            'total' => 0,
            'errors' => 0
        ];

        $this->attachListeners($output, $counter);

        $this->sendPaymentReminderProcessor->process();

        $tag = $counter['errors'] === 0 ? 'info' : 'error';
        $output->writeln(sprintf(
            '<%1$s>%2$d send payment reminder processed (%3$d errors).</%1$s>',
            $tag,
            $counter['total'],
            $counter['errors']
        ));

        return $counter['errors'] === 0 ? self::SUCCESS : self::FAILURE;
    }

    protected function attachListeners(OutputInterface $output, &$counter)
    {
        $this->dispatcher->addListener(
            SendEvents::PAYMENT_REMINDER_PROCESS_INITIALIZE,
            function (SendPaymentReminderEvent $event) use ($output, &$counter) {
                $output->write(sprintf(
                    '<comment>Processing send payment reminder #%s...</comment>',
                    $event->getApplicationFormNumber()
                ));
                $counter['total']++;
            }
        );

        $this->dispatcher->addListener(
            SendEvents::PAYMENT_REMINDER_PROCESS_COMPLETED,
            function (SendPaymentReminderEvent $event) use ($output) {
                $output->writeln('<info>OK</info>');
            }
        );

        $this->dispatcher->addListener(
            SendEvents::PAYMENT_REMINDER_PROCESS_ERROR,
            function (SendPaymentReminderEvent $event) use ($output, &$counter) {
                $output->writeln(sprintf('<error>ERROR (%s)</error>', $event['exception']->getMessage()));
                $counter['errors']++;
            }
        );
    }
}
