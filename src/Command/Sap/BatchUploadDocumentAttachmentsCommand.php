<?php

namespace App\Command\Sap;

use App\Command\Command;
use App\Event\SapDocumentAttachmentEvent;
use App\Event\SapEvents;
use App\Processor\SapDocumentAttachmentProcessor;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class BatchUploadDocumentAttachmentsCommand extends Command
{
    protected static $defaultName = 'app:sap:batch-upload-document-attachments';

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var SapDocumentAttachmentProcessor
     */
    private $sapDocumentAttachmentProcessor;

    public function __construct(
        EventDispatcherInterface $dispatcher,
        SapDocumentAttachmentProcessor $sapDocumentAttachmentProcessor
    ) {
        $this->dispatcher = $dispatcher;
        $this->sapDocumentAttachmentProcessor = $sapDocumentAttachmentProcessor;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Batch upload document attachments to SAP')
            ->addOption('stop-on-error', null, null, 'Stop processing upon first error')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->sapDocumentAttachmentProcessor->setStopOnError($input->getOption('stop-on-error'));

        $output->writeln('Processing documents...');

        $counter = [
            'total' => 0,
            'skipped' => 0,
            'errors' => 0
        ];

        $this->attachListeners($output, $counter);

        $this->sapDocumentAttachmentProcessor->process();

        $tag = $counter['errors'] === 0 ? 'info' : 'error';
        $output->writeln(sprintf(
            '<%1$s>%2$s documents processed (%3$s errors).</%1$s>',
            $tag,
            $counter['total'],
            $counter['errors']
        ));

        return $counter['errors'] === 0 ? self::SUCCESS : self::FAILURE;
    }

    protected function attachListeners(OutputInterface $output, &$counter)
    {
        $this->dispatcher->addListener(
            SapEvents::DOCUMENT_ATTACHMENT_PROCESS_INITIALIZE,
            function (SapDocumentAttachmentEvent $event) use ($output, &$counter) {
                $output->write(sprintf(
                    '<comment>Processing document #%s...</comment>',
                    $event->getJournalEntry()->getDocumentNumber()
                ));
                $counter['total']++;
            }
        );

        $this->dispatcher->addListener(
            SapEvents::DOCUMENT_ATTACHMENT_PROCESS_COMPLETED,
            function (SapDocumentAttachmentEvent $event) use ($output) {
                $output->writeln('<info>OK</info>');
            }
        );

        $this->dispatcher->addListener(
            SapEvents::DOCUMENT_ATTACHMENT_PROCESS_SKIPPED,
            function (SapDocumentAttachmentEvent $event) use ($output, &$counter) {
                $output->writeln('<comment>SKIP</comment>');
                $counter['skipped']++;
            }
        );

        $this->dispatcher->addListener(
            SapEvents::DOCUMENT_ATTACHMENT_PROCESS_ERROR,
            function (SapDocumentAttachmentEvent $event) use ($output, &$counter) {
                $output->writeln(sprintf('<error>ERROR (%s)</error>', $event['exception']->getMessage()));
                $counter['errors']++;
            }
        );
    }
}
