<?php

namespace App\Validator\Constraints;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Checks whether a value is required depending on country.
 */
class CityValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        $object = $this->context->getObject();
        $invoiceFields = ['invoiceCity', 'invoiceCityOther'];
        if (in_array($this->context->getPropertyName(), $invoiceFields)) {
            $country = PropertyAccess::createPropertyAccessor()
                ->getValue($object, 'invoiceCountry');
        } else {
            $country = PropertyAccess::createPropertyAccessor()
                ->getValue($object, 'country');
        }

        $cityFields = ['city', 'invoiceCity'];
        $cityOtherFields = ['cityOther', 'invoiceCityOther'];
        if (
            (in_array($this->context->getPropertyName(), $cityFields)  && $country == 'SK' && empty($value))
            || (in_array($this->context->getPropertyName(), $cityOtherFields) && $country != 'SK' && empty($value))
        ) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(City::CITY_REQUIRED)
                ->addViolation();
        }
    }
}
