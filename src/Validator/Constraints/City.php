<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class City extends Constraint
{
    const CITY_REQUIRED = '7f900c12-61bd-455d-9398-996cd040f7f9';

    protected static $errorNames = [
        self::CITY_REQUIRED => 'CITY_REQUIRED',
    ];

    public $message = 'Please select city';
}
